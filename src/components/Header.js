'use strict';

import React, { Component } from 'react';
import { StyleSheet, Text, TextInput, View, Button, ActivityIndicator, Image} from 'react-native';

export default class Header extends Component<Props> {
  render() {
    return (
      <View style={{flex: 1}}>
        <Text style={styles.description}>
          Search
        </Text>
        <View style={{height: 100, flex: 5/10, alignSelf: 'stretch'}}>
          <Image style={{flex: 1}} source={{ uri: 'https://www.youtube.com/yts/img/yt_1200-vfl4C3T0K.png'}} />
        </View>
        <Text style={styles.videosText}>
          Videos
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  description: {
    // marginBottom: 20,
    fontSize: 28,
    textAlign: 'center',
    color: '#48BBEC',
    marginTop: '30%',
    marginBottom: 3
  },
  videosText: {
    // marginBottom: 20,
    fontSize: 28,
    textAlign: 'center',
    color: '#48BBEC',
    marginTop: 3
  },
});