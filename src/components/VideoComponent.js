'use strict';

import React, { Component } from 'react';
import { StyleSheet, Text, TextInput, View, Button, ActivityIndicator, Image} from 'react-native';

export default class VideoComponent extends Component<Props> {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.description}>
          Video Component
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  description: {
    // marginBottom: 20,
    justifyContent: 'center',
    fontSize: 18,
    textAlign: 'center',
    color: '#656565'
  },
  container: {
    flex: 1,
    alignItems: 'stretch',
    justifyContent: 'center',
  },
});