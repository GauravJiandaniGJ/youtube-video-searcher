'use strict';

import React, { Component } from 'react';
import { StyleSheet, Text, TextInput, WebView, TouchableHighlight, View, Button, ActivityIndicator, Image} from 'react-native';
import FontAwesome, { Icons } from 'react-native-fontawesome';

export default class SettingsScreen extends Component<Props> {
  render() {
    return (
      <View style={styles.container}>
        <TouchableHighlight onPress={() => this.props.navigation.navigate('Home')}><FontAwesome size={20} style={{alignSelf: 'flex-start', paddingLeft: 5, fontSize: 24, paddingTop: 3, color: '#48BBEC'}}>{Icons.angleLeft}</FontAwesome></TouchableHighlight>
        <View style={{justifyContent: 'center', flex: 1}}>
          <WebView
              source = {{ uri: 
                'https://www.google.com/' }}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  description: {
    // marginBottom: 20,
    fontSize: 18,
    textAlign: 'center',
    // color: '#656565',
    justifyContent: 'center',
    color: '#48BBEC'
  },
  container: {
    flex: 1,
    alignItems: 'stretch',
  },
});