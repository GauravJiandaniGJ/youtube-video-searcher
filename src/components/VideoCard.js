'use strict';

import React, { Component } from 'react';
import { StyleSheet, Text, Image, TextInput, View, TouchableOpacity, Button, ActivityIndicator} from 'react-native';
import FontAwesome, { Icons } from 'react-native-fontawesome';

export default class VideoCard extends Component {
checkForNull(obj) {
    if(obj && obj !== null && obj !== undefined && obj !== '') {
        return true
    } else {
        return false
    }
}  
render() {
    return (
      <TouchableOpacity style = {styles.container2} onPress={() => this.props.navigation.navigate('VideoScreen', { videoObject: this.props.item })}>
        <View style = {styles.blackImg}>
            {this.props.item && this.props.item.snippet && this.props.item.snippet !== undefined && this.props.item.snippet.thumbnails !== undefined && this.props.item.snippet.thumbnails !== null && this.props.item.snippet.thumbnails.default !== null && <Image style = {styles.img} source = {{ uri: this.props.item.snippet.thumbnails.default.url}} />}
            <FontAwesome color="white" size={50} style={{ position: 'absolute', fontSize: 20, alignSelf: 'center', justifyContent: 'center', flexDirection: 'row', top: '50%' }}>{Icons.playCircleO}</FontAwesome>
        </View>
        <View style = {styles.content}>
            {this.checkForNull(this.props.item) && this.checkForNull(this.props.item.snippet) && this.checkForNull(this.props.item.snippet.title) && this.props.item.snippet.title.length > 30 && <Text style={{marginLeft: '5%', fontSize: 16, fontWeight: '700'}}>{this.props.item.snippet.title.substring(0, 30)+'...'}</Text>}
            {this.checkForNull(this.props.item) && this.checkForNull(this.props.item.snippet) && this.checkForNull(this.props.item.snippet.title) && this.props.item.snippet.title.length < 30 && <Text style={{marginLeft: '5%', fontSize: 16, fontWeight: '700'}}>{this.props.item.snippet.title}</Text>}
            {this.checkForNull(this.props.item) && this.checkForNull(this.props.item.snippet) && this.checkForNull(this.props.item.snippet.channelTitle) && <Text style={{marginLeft: '5%', fontSize: 12, fontWeight: '400'}}>{this.props.item.snippet.channelTitle}</Text>}
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
    container2: {
        marginBottom: 20,
        // flexDirection: 'row',
        // backgroundColor: '#f5f5f5'
        justifyContent: 'space-between',
        // alignItems: 'center',
        alignSelf: 'stretch',
        width: '50%',
        borderRightWidth: 2,
        borderRightColor: 'white',
        borderLeftWidth: 2,
        borderLeftColor: 'white',
        backgroundColor: 'white'
        // borderBottomWidth: 1,
        // borderBottomColor: '#f4c842'
     },
     blackImg: {
        // backgroundColor: 'red',
        height: 120,
        // width: 120
        flex: 4/10
     },
     img: {
         flex: 1
     },
     content: {
         flex: 6/10,
         backgroundColor: '#e4e4e4',
         height: 80,
     }
});