'use strict';

import React, { Component } from 'react';
import { StyleSheet, Text, TextInput, View, TouchableHighlight, ScrollView, Button, ActivityIndicator, Image} from 'react-native';
import Header from './Header';
import SearchBox from './SearchBox';
import FontAwesome, { Icons } from 'react-native-fontawesome';
import VideoScreen from './VideoScreen';
import VideoCard from './VideoCard';

// import { Icon, Button, Container, Header, Content, Left } from 'native-base';

export default class HomeScreen extends Component {

  state = {
    searchSuccess: false,
    searchedData: {},
    dataReceived: false,
    loading: false
  };

  searchIsSuccess(dataReceived) {
    this.setState({searchSuccess: true, searchedData: dataReceived, dataReceived: true, loading: false});
    // console.log('HomeScreen', dataReceived);
  }

  loadingStarted() {
    this.setState({loading: true})
  }

  resetEverything() {
    this.setState({searchSuccess: false, searchedData: {}, dataReceived: false, loading: false})
  }

  render() {
    return (
      <ScrollView style={{backgroundColor: 'white'}}>
        <TouchableHighlight onPress={() => this.props.navigation.navigate('DrawerOpen')} style={{display: 'flex'}}>
          <Text style={{fontSize: 17, padding: 6, height: 35, backgroundColor: '#F0F0F0',textAlign: 'left', borderRadius: 0, borderBottomWidth: 1, borderBottomColor: '#F0F0F0', color: '#48BBEC'}}>
            <FontAwesome style={{alignSelf: 'center'}}>{Icons.bars}</FontAwesome>
            <Text>{'  '}Video Finder</Text>
          </Text>
        </TouchableHighlight>
        <SearchBox searchIsSuccess={this.searchIsSuccess.bind(this)} loadingStarted={this.loadingStarted.bind(this)} resetEverything={this.resetEverything.bind(this)}/>
        {
          this.state.searchSuccess
            ?
              <View style={{display: 'flex', flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'space-between', backgroundColor: 'white'}}>
                {
                  this.state.dataReceived
                    ?
                      this.state.searchedData.items.map((item, key) => {
                        return (<VideoCard key={key} item={item} navigation={this.props.navigation}/>);
                      })
                    :
                      null
                }
              </View>
            :
              <View>
                {
                  this.state.loading
                    ?
                      <Text style={{fontSize: 18, textAlign: 'center', color: '#656565', marginTop: '50%'}}>
                        <FontAwesome style={{alignSelf: 'center'}}>{Icons.spinner}{' Loading...'}</FontAwesome>
                      </Text>
                    :
                      <Header />
                }
              </View>
        }
        {/* <VideoScreen /> */}
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  description: {
    // marginBottom: 20,
    fontSize: 18,
    textAlign: 'center',
    color: '#656565'
  },
});