'use strict';

import React, { Component } from 'react';
import { StyleSheet, Keyboard, Text, TextInput, View, Button, ActivityIndicator, Image} from 'react-native';

export default class SearchBox extends Component<Props> {
  state = {
    searching: '',
    searchDone: false
  }
  searchVideo() {
    this.props.loadingStarted();
    Keyboard.dismiss()
    let temp = {};
    // const tryTemp = this.props.searchIsSuccess(temp);
    const url = 'https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=15&q='+this.state.searching+'&key=AIzaSyBBsdfSVG06rAXAYEXucDtIyzIZMtNWs1g'
    fetch(url).then(function(response) {
      if (response.status >= 400) {
        throw new Error("Bad response from server");
      }
      return response.json();
    })
    .then(function(data) {
      temp = Object.assign({}, data);
      if(temp !== null && temp !== undefined && temp.items !== undefined && temp.items.length > 0) {
        this.props.searchIsSuccess(temp);
        this.setState({searchDone: true})
      }
    }.bind(this));
  }
  resetEverything() {
    this.setState({searching: '', searchDone: false});
    this.props.resetEverything();
  }
  render() {
    return (
      <View style={styles.flowRight}>
        <TextInput
            value={this.state.searching}
            onChangeText={(searching) => this.setState({searching})}
            underlineColorAndroid={'transparent'}
            style={styles.searchInput}
            clearTextOnFocus={true}
            placeholderTextColor={'#48BBEC'}
            placeholder='Enter Video Name..'
        />
        {
          this.state.searchDone
            ?
            <Button
                onPress={this.resetEverything.bind(this)}
                style={styles.goButton}
                color='#48BBEC'
                title='Reset'
            />
            :
            <Button
                onPress={this.searchVideo.bind(this)}
                style={styles.goButton}
                color='#48BBEC'
                title='Go'
            />
        }
        <View style={{paddingRight: 5 }}>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
    flowRight: {
        paddingTop: 5,
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf: 'stretch',
        justifyContent: 'space-between',
        marginTop: 10,
        marginBottom: 20
      },
      searchInput: {
        height: 36,
        padding: 6,
        marginLeft: 5,
        marginRight: 5,
        flexGrow: 1,
        fontSize: 18,
        borderWidth: 1,
        borderBottomWidth: 1.5,
        borderColor: '#48BBEC',
        borderRadius: 8,
        color: '#48BBEC',
      },
      goButton: {
        paddingRight: 15,
        flexGrow: 1,
        borderRadius: 8
      }
});