'use strict';

import React, { Component } from 'react';
import { StyleSheet, Text, TextInput, Linking, TouchableOpacity, TouchableHighlight, View, Button, ActivityIndicator, Image} from 'react-native';
import Video from 'react-native-video';
import YouTube from 'react-native-youtube'
import FontAwesome, { Icons } from 'react-native-fontawesome';
import SystemSetting from 'react-native-system-setting'

export default class VideoScreen extends Component<Props> {
  state = {
    mutedVideo: false,
    previousVolume: 0
  };

  checkForNull(item) {
    if(item && item !== null && item !== undefined) {
      return true
    } else {
      return false
    }
  }
  componentWillReceiveProps(nextProps) {
    console.log('rece props', nextProps);
    if(!!this.props.navigation.state.params && !!this.props.navigation.state.params.videoObject.snippet.description) {
      this.checkIfLinkExist(this.props.navigation.state.params.videoObject.snippet.description);
    }
  }
  muteVideo() {
    // console.log('mute audio');
    //   SystemSetting.getVolume().then((volume)=>{
    //     console.log('Current volume is ' + volume);
    // });
    // SystemSetting.setBrightnessForce(0.5);
    SystemSetting.getVolume().then((volume)=>{
      this.setState({previousVolume: volume})
    });
    SystemSetting.setVolume(0.05, {
      type: 'music',
      playSound: false,
      showUI: false
    });
    this.setState({mutedVideo: true})
  }
  unMuteVideo() {
    SystemSetting.setVolume(this.state.previousVolume, {
      type: 'music',
      playSound: false,
      showUI: false
    });
    this.setState({mutedVideo: false})
  }
  checkIfLinkExist(str) {
    if(str !== '' && str.indexOf('http') >= 0) {
      var temp = str;
      var count = (temp.match(/http/g) || []).length;
      const stringToBeChanged = '';
      const lastIndexHttp = 0;
      // let finalString = '';
      for(let i = 0; i < count; i++) {
        stringToBeChanged = str.substring(str.indexOf('http',lastIndexHttp),str.indexOf(' ', str.indexOf('http', lastIndexHttp)));
        lastIndexHttp = str.indexOf('http') + 4;
        str = str.replace(stringToBeChanged, this.replaceString(stringToBeChanged))
        console.log(str);
      }
      return str;
    }
  }
  replaceString(str) {
    // console.log('rep called', str);
    return (
    <Text style={{color: 'blue'}} onPress={() => Linking.openURL(str)}>
      {str}
    </Text>
    );
  }
  render() {
    if(!!this.props.navigation.state.params && !!this.props.navigation.state.params.videoObject.snippet.description) {
      const temp = this.checkIfLinkExist(this.props.navigation.state.params.videoObject.snippet.description);
    }
    // console.log(this.props.navigation.state.params.videoObject, 'props in video screen');
    // const url = 'https://www.youtube.com/watch?v=srl0cnKzGIw&index=27&list=RDMMyIIGQB6EMAM';
    return (
      this.checkForNull(this.props.navigation) && this.checkForNull(this.props.navigation.state.params) && this.checkForNull(this.props.navigation.state.params.videoObject)
        ?
          <View style={styles.container}>
            <TouchableHighlight onPress={() => this.props.navigation.navigate('Home')}><FontAwesome size={20} style={{alignSelf: 'flex-start', paddingLeft: 5, fontSize: 24, paddingTop: 3, color: '#48BBEC'}}>{Icons.angleLeft}</FontAwesome></TouchableHighlight>
            <Text style={styles.description}>
              Video Screen
            </Text>
            {
              this.props.navigation.state.params.videoObject.id !== null && this.props.navigation.state.params.videoObject.id !== undefined && this.props.navigation.state.params.videoObject.id.videoId !== undefined && this.props.navigation.state.params.videoObject.id.videoId !== ''
                ?
                  <View>
                    <YouTube
                      apiKey="AIzaSyCPWky0_3R5nk8QtpVZvjmwxB842t5680Q"
                      videoId={this.props.navigation.state.params.videoObject.id.videoId}   // The YouTube video ID
                      play={false}             // control playback of video with true/false
                      fullscreen={false}       // control whether the video should play in fullscreen or inline
                      loop={true}
                      style={{ alignSelf: 'stretch', height: 280 }}
                    />
                    <Text style={{marginLeft: '3%', marginTop: 8, fontSize: 18, fontWeight: '700'}}>
                      {this.props.navigation.state.params.videoObject.snippet.title}
                    </Text>
                    <Text style={{marginLeft: '3%', marginTop: 8, fontSize: 14, fontWeight: '400'}}>
                      {this.props.navigation.state.params.videoObject.snippet.description}
                    </Text>
                      <View style={{display: 'flex', flexDirection: 'row', justifyContent: 'space-between'}}>
                          {
                            this.state.mutedVideo
                              ?
                                <TouchableHighlight style={{marginLeft: '3%'}} onPress={this.unMuteVideo.bind(this)}>
                                  <Text style={{alignSelf: 'flex-start', marginTop: 8, fontSize: 18, fontWeight: '400'}}>
                                    <FontAwesome>{Icons.volumeOff}</FontAwesome>
                                  </Text>
                                </TouchableHighlight>
                              :
                                <TouchableHighlight style={{marginLeft: '3%'}} onPress={this.muteVideo.bind(this)}>
                                  <Text style={{alignSelf: 'flex-start', marginTop: 8, fontSize: 18, fontWeight: '400'}}>
                                    <FontAwesome>{Icons.volumeUp}</FontAwesome>
                                  </Text>    
                                </TouchableHighlight>
                          }
                        <Text style={{marginRight: '3%', alignSelf: 'flex-end', marginTop: 8, fontSize: 18, fontWeight: '400'}}>
                          {this.props.navigation.state.params.videoObject.snippet.channelTitle}
                        </Text>
                      </View>
                    </View>
                :
                  null
            }
          </View>
        :
          <View style={styles.container}>
            <TouchableHighlight onPress={() => this.props.navigation.navigate('Home')}><FontAwesome size={20} style={{alignSelf: 'flex-start', paddingLeft: 5, fontSize: 24, paddingTop: 3, color: '#48BBEC'}}>{Icons.angleLeft}</FontAwesome></TouchableHighlight>
            <Text style={styles.noData}>
              Can't Play the Video! Data Inadequate
            </Text>
          </View>
    );
  }
}

const styles = StyleSheet.create({
  description: {
    marginBottom: 10,
    justifyContent: 'center',
    fontSize: 18,
    textAlign: 'center',
    // color: '#656565'
    color: '#48BBEC'
  },
  noData: {
    marginTop: 40,
    justifyContent: 'center',
    fontSize: 18,
    textAlign: 'center',
    color: '#48BBEC'
  },
  container: {
    flex: 1,
    marginTop: 10,
    backgroundColor: '#f5f5f5'
  },
});