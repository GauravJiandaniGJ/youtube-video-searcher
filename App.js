/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
'use strict';

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Button,
  Icon
} from 'react-native';
import HomeScreen from './src/components/HomeScreen';
import SettingsScreen from './src/components/SettingsScreen';
import VideoScreen from './src/components/VideoScreen';
import PlexusMDScreen from './src/components/PlexusMDScreen';
import {DrawerNavigator} from 'react-navigation';

type Props = {};
export default class App extends Component<Props> {
  render() {
    return (
      <MyApp navigation={this.props.navigation}/>
      // {/* <View style={styles.container}>
      //   <Text style={styles.welcome}>
      //     Welcome to React
      //   </Text>
      //   <Text style={styles.instructions}>
      //     To get started, edit App.js
      //   </Text>
      //   <Text style={styles.instructions}>
      //     {instructions}
      //   </Text>
      // </View> */}
    );
  }
}


const MyApp = DrawerNavigator({
  Home: {
    screen: HomeScreen,
    navigationOptions: {
      title: 'Home',  // Title to appear in status bar
      // headerLeft: <Icon name="menu" size={35} onPress={ () => this.props.navigation.navigate('DrawerOpen') } />
    }
  },
  VideoScreen: {
    screen: VideoScreen,
    navigationOptions: {
      title: 'Last Played Video'
    }
  },
  Settings: {
    screen: SettingsScreen,
    navigationOptions: {
      title: 'Google Search',  // Title to appear in status bar
      // headerLeft: <Icon name="menu" size={35} onPress={ () => this.props.navigation.navigate('DrawerOpen') } />
    }
  },
  PlexusMD: {
    screen: PlexusMDScreen,
    navigationOptions: {
      title: 'PlexusMD'
    }
  }
})


const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  }
});
